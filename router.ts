import express from 'express';
import * as DigimonsController from './src/controllers/DigimonsController';
import * as PokemonsController from './src/controllers/PokemonController';


export const router = express.Router();

router.get('/', (req, res) => {
    res.send('Hello World with Typescript!')
})

router.get('/ts', (req, res) => {
    res.send('Typescript es lo máximo!')
})

router.get('/digimons', DigimonsController.getAll);
router.get('/digimons/:id', DigimonsController.get);
router.get('/digimons/getByType/:type', DigimonsController.getByType);
router.get('/digimons/stronger/:digimon1/:digimon2', DigimonsController.stronger);
router.get('/digimons/findByName/:name', DigimonsController.findByName);
router.post("/digimons/", (req, res) => {
    res.status(200).send(req.body);
});
router.get('/pokemons', PokemonsController.getAll);
router.get('/pokemons/:id', PokemonsController.get);
router.get('/pokemons/getByType/:type', PokemonsController.getByType);
router.get('/pokemons/stronger/:pokemon1/:pokemon2', PokemonsController.stronger);
router.get('/pokemons/findByName/:name', PokemonsController.findByName);
router.post("/pokemons/", (req, res) => {
    res.status(200).send(req.body);
});
