import { Request, Response } from "express";
import PokemonsService from "../services/PokemonsService";

export function getAll(_: any, res: Response) {
    console.log("pokemons")
    const pokemons = PokemonsService.getAll();
    res.status(200).json(pokemons);
}

export function get(req: Request, res: Response) {
    try {
        const id = req.params.id && +req.params.id || undefined;
        if (!id) { throw "Se requiere el ID del digimon." }
        const digimon = PokemonsService.get(id);
        res.status(200).json(digimon);
    } catch (error) {
        res.status(400).send(error);
    }
}
export function getByType(req: Request, res: Response) {
    try {
        const type = req.params.type;
        if (!type) { throw "Se requiere el tipo del digimon." }
        const digimon = PokemonsService.getByType(type);
        res.status(200).json(digimon);
    } catch (error) {
        res.status(400).send(error);
    }
}
export function findByName(req: Request, res: Response) {
    try {
        const name = req.params.name || undefined;
        if (!name) {
            throw "Ingrese otro nombre";
        }
        const digimon = PokemonsService.findByName(name);
        res.status(200).json(digimon);
    } catch (error) {
        res.status(400).send(error);
    }
}
export function stronger(req: Request, res: Response) {
    try {
        const pokemon1 = req.params.digimon1;
        const pokemon2 = req.params.digimon2;

        if (!pokemon1 || !pokemon2) { throw "Se requiere el tipo del digimon." }
        const pokemon = PokemonsService.stronger(pokemon1, pokemon2);
        res.status(200).json(pokemon);
    } catch (error) {
        res.status(400).send(error);
    }
}