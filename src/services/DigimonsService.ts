import { DigimonI } from "../interfaces/DigimonInterfaces";
const db = require('../db/Digimons.json');

module DigimonsService {
    export function getAll(): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        return digimons
    }
    export function get(id: number): DigimonI {
        const digimons: Array<DigimonI> = db;
        const digimon: Array<DigimonI> = digimons.filter(e => e.id === id);
        if (digimon.length < 1) {
            throw "No se encontró el digimon"
        }
        return digimon[0];
    }
    export function getByType(type: string): DigimonI[] {
        const digimons: Array<DigimonI> = db;
        const digimon: Array<DigimonI> =
            digimons.filter(e => e.type.filter(element => {
                element.name === type
            }));
        if (digimon.length < 1) {
            throw "No se encontró el digimon"
        }
        return digimon;
    }
    export function stronger(digimon1name: string, digimon2name: string): string {
        const digimons: Array<DigimonI> = db;
        const digimon1: DigimonI = digimons.find(e => e.name === digimon1name) as DigimonI;
        const digimon2: DigimonI = digimons.find(e => e.name === digimon2name) as DigimonI;
        var returna = "";
        if (digimon1 === null || digimon2 === null) {
            throw "No se encontró el digimon"
        }
        digimon2.type.forEach((resp) => {
            console.log(resp)
            resp.weakAgainst.forEach((element) => {
                console.log(element)
                const algo = digimon1.type.filter(e => e.strongAgainst.includes(element))
                console.log(algo);
                if (algo.length === 0) {
                    returna = 'Mas Poderoso es: ' + digimon1name;
                } else {
                    returna = 'Mas Poderoso es: ' + digimon2name;
                }
                const algo1 = digimon1.type.filter(e => e.strongAgainst.includes(element))

            })
        })
        return returna;
    }
    export function findByName(name: string): DigimonI {
        const digimons: Array<DigimonI> = db;
        const digimon: DigimonI | undefined = digimons.find((e) =>
            e.name.match(name)
        ) as DigimonI;
        if (!digimon) {
            throw "No hay digimon";
        }
        return digimon;
    }
}

export default DigimonsService;
