import { PokemonI } from "../interfaces/PokemonInterfaces";
const db = require('../db/Pokemons.json');

module PokemonsService {
    export function getAll(): Array<PokemonI> {
        console.log("pokemons")
        const pokemons: Array<PokemonI> = db;
        return pokemons
    }
    export function get(id: number): PokemonI {
        const pokemons: Array<PokemonI> = db;
        const pokemon: Array<PokemonI> = pokemons.filter(e => e.id === id);
        if (pokemon.length < 1) {
            throw "No se encontró el digimon"
        }
        return pokemon[0];
    }
    export function getByType(type: string): PokemonI[] {
        const pokemons: Array<PokemonI> = db;
        const pokemon: Array<PokemonI> =
            pokemons.filter(e => e.type.filter(element => {
                element.name === type
            }));
        if (pokemon.length < 1) {
            throw "No se encontró el digimon"
        }
        return pokemon;
    }
    export function stronger(digimon1name: string, digimon2name: string): string {
        const pokemons: Array<PokemonI> = db;
        const pokemon1: PokemonI = pokemons.find(e => e.name === digimon1name) as PokemonI;
        const pokemon2: PokemonI = pokemons.find(e => e.name === digimon2name) as PokemonI;
        var returna = "";
        if (pokemon1 === null || pokemon2 === null) {
            throw "No se encontró el digimon"
        }
        pokemon2.type.forEach((resp) => {
            console.log(resp)
            resp.weakAgainst.forEach((element) => {
                console.log(element)
                const algo = pokemon1.type.filter(e => e.strongAgainst.includes(element))
                console.log(algo);
                if (algo.length === 0) {
                    returna = 'Mas Poderoso es: ' + digimon1name;
                } else {
                    returna = 'Mas Poderoso es: ' + digimon2name;
                }
                const algo1 = pokemon1.type.filter(e => e.strongAgainst.includes(element))

            })
        })
        return returna;
    }
    export function findByName(name: string): PokemonI {
        const pokemons: Array<PokemonI> = db;
        const pokemon: PokemonI | undefined = pokemons.find((e) =>
            e.name.match(name)
        ) as PokemonI;
        if (!pokemon) {
            throw "No hay digimon";
        }
        return pokemon;
    }
}

export default PokemonsService;
